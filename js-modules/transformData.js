var fs = require('fs');

var fileName = './JSON/data.json';

fs.readFile(fileName, 'utf8', (err, fileData) => {
    var data = JSON.parse(fileData);

    var teamWins2015 = [];
    var teamWins2016 = [];
    var teamWins2017 = [];

    var teamTotalRuns2015 = [];
    var teamTotalRuns2016 = [];
    var teamTotalRuns2017 = [];

    var topBatsmenRuns2015 = [];
    var topBatsmenRuns2016 = [];
    var topBatsmenRuns2017 = [];

    var bestStrikeRates2015 = [];
    var bestStrikeRates2016 = [];
    var bestStrikeRates2017 = [];

    var bestEconomy2015 = [];
    var bestEconomy2016 = [];
    var bestEconomy2017 = [];

    var mostWickets2015 = [];
    var mostWickets2016 = [];
    var mostWickets2017 = [];

    for (k in data['2015'].teamsWinsAndScores) {
        if(data['2015'].teamsWinsAndScores.hasOwnProperty(k)) {
            let temp = [];
            temp.push(k);
            temp.push(data['2015'].teamsWinsAndScores[k].wins);
            teamWins2015.push(temp);
            let runs = [];
            runs.push(k);
            runs.push(data['2015'].teamsWinsAndScores[k].teamsTotalRuns);
            teamTotalRuns2015.push(runs);
        }
    }

    for (k in data['2016'].teamsWinsAndScores) {
        if(data['2016'].teamsWinsAndScores.hasOwnProperty(k)) {
            let temp = [];
            temp.push(k);
            temp.push(data['2016'].teamsWinsAndScores[k].wins);
            teamWins2016.push(temp);
            let runs = [];
            runs.push(k);
            runs.push(data['2016'].teamsWinsAndScores[k].teamsTotalRuns);
            teamTotalRuns2016.push(runs);
        }
    }

    for (k in data['2017'].teamsWinsAndScores) {
        if(data['2017'].teamsWinsAndScores.hasOwnProperty(k)) {
            let temp = [];
            temp.push(k);
            temp.push(data['2017'].teamsWinsAndScores[k].wins);
            teamWins2017.push(temp);
            let runs = [];
            runs.push(k);
            runs.push(data['2017'].teamsWinsAndScores[k].teamsTotalRuns);
            teamTotalRuns2017.push(runs);
        }
    }


    for (k in data['2015'].bowlersEconomyAndWickets) {
        if(data['2015'].bowlersEconomyAndWickets.hasOwnProperty(k)) {
            if(data['2015'].bowlersEconomyAndWickets[k].economyRates < 7 && data['2015'].bowlersEconomyAndWickets[k].totalBalls > 30) {
                let temp = [];
                temp.push(k);
                temp.push(data['2015'].bowlersEconomyAndWickets[k].economyRates);
                bestEconomy2015.push(temp);
            }
        if(data['2015'].bowlersEconomyAndWickets[k].totalWickets > 17 ) {
                let wickets = [];
                wickets.push(k);
                wickets.push(data['2015'].bowlersEconomyAndWickets[k].totalWickets);
                mostWickets2015.push(wickets);
            }
        }
    }

        for (k in data['2016'].bowlersEconomyAndWickets) {
        if(data['2016'].bowlersEconomyAndWickets.hasOwnProperty(k)) {
            if(data['2016'].bowlersEconomyAndWickets[k].economyRates < 7 && data['2016'].bowlersEconomyAndWickets[k].totalBalls > 30) {
                let temp = [];
                temp.push(k);
                temp.push(data['2016'].bowlersEconomyAndWickets[k].economyRates);
                bestEconomy2016.push(temp);
            }
        if(data['2016'].bowlersEconomyAndWickets[k].totalWickets > 14 ) {
                let wickets = [];
                wickets.push(k);
                wickets.push(data['2016'].bowlersEconomyAndWickets[k].totalWickets);
                mostWickets2016.push(wickets);
            }
        }
    }

    for (k in data['2017'].bowlersEconomyAndWickets) {
        if(data['2017'].bowlersEconomyAndWickets.hasOwnProperty(k)) {
            if(data['2017'].bowlersEconomyAndWickets[k].economyRates < 6.5 && data['2017'].bowlersEconomyAndWickets[k].totalBalls > 30) {
                let temp = [];
                temp.push(k);
                temp.push(data['2017'].bowlersEconomyAndWickets[k].economyRates);
                bestEconomy2017.push(temp);
            }
        if(data['2017'].bowlersEconomyAndWickets[k].totalWickets > 16 ) {
                let wickets = [];
                wickets.push(k);
                wickets.push(data['2017'].bowlersEconomyAndWickets[k].totalWickets);
                mostWickets2017.push(wickets);
            }
        }
    }

    for (k in data['2015'].batsmenRunsAndStrikeRate) {
        if(data['2015'].batsmenRunsAndStrikeRate.hasOwnProperty(k)) {
            if(data['2015'].batsmenRunsAndStrikeRate[k].strikeRate > 156 && data['2015'].batsmenRunsAndStrikeRate[k].totalBalls > 75 ) {
                let temp = [];
                temp.push(k);
                temp.push(data['2015'].batsmenRunsAndStrikeRate[k].strikeRate);
                bestStrikeRates2015.push(temp);
            }
        if(data['2015'].batsmenRunsAndStrikeRate[k].totalRuns > 430 ) {
                let runs = [];
                runs.push(k);
                runs.push(data['2015'].batsmenRunsAndStrikeRate[k].totalRuns);
                topBatsmenRuns2015.push(runs);
            }
        }
    }

    for (k in data['2016'].batsmenRunsAndStrikeRate) {
            if(data['2016'].batsmenRunsAndStrikeRate.hasOwnProperty(k)) {
                if(data['2016'].batsmenRunsAndStrikeRate[k].strikeRate > 150 && data['2016'].batsmenRunsAndStrikeRate[k].totalBalls > 60 ) {
                    let temp = [];
                    temp.push(k);
                    temp.push(data['2016'].batsmenRunsAndStrikeRate[k].strikeRate);
                    bestStrikeRates2016.push(temp);
                }
            if(data['2016'].batsmenRunsAndStrikeRate[k].totalRuns > 480 ) {
                    let runs = [];
                    runs.push(k);
                    runs.push(data['2016'].batsmenRunsAndStrikeRate[k].totalRuns);
                    topBatsmenRuns2016.push(runs);
                }
            }
        }

    for (k in data['2017'].batsmenRunsAndStrikeRate) {
            if(data['2017'].batsmenRunsAndStrikeRate.hasOwnProperty(k)) {
                if(data['2017'].batsmenRunsAndStrikeRate[k].strikeRate > 155 && data['2017'].batsmenRunsAndStrikeRate[k].totalBalls > 75 ) {
                    let temp = [];
                    temp.push(k);
                    temp.push(data['2017'].batsmenRunsAndStrikeRate[k].strikeRate);
                    bestStrikeRates2017.push(temp);
                }
            if(data['2017'].batsmenRunsAndStrikeRate[k].totalRuns > 395 ) {
                    let runs = [];
                    runs.push(k);
                    runs.push(data['2017'].batsmenRunsAndStrikeRate[k].totalRuns);
                    topBatsmenRuns2017.push(runs);
                }
            }
        }

    var dataObj = new Object();
    dataObj['teamWins2015'] = new Object();
    dataObj['teamWins2015']['data'] = teamWins2015;
    dataObj['teamWins2015']['title'] = 'Teams wins the IPL season 2015'
    dataObj['teamWins2015']['subtitle'] = 'Number of wins of each team in IPL season 2015';
    dataObj['teamWins2015']['tooltip'] = 'won <b>{point.y:.1f} matches in IPL 2015</b>';
    dataObj['teamWins2015']['seriesName'] = 'Team Wins';

    dataObj['teamWins2016'] = new Object();
    dataObj['teamWins2016']['data'] = teamWins2016;
    dataObj['teamWins2016']['title'] = 'Teams wins the IPL season 2016'
    dataObj['teamWins2016']['subtitle'] = 'Number of wins of each team in IPL season 2016';
    dataObj['teamWins2016']['tooltip'] = 'won <b>{point.y:.1f} matches in IPL 2016</b>';
    dataObj['teamWins2016']['seriesName'] = 'Team Wins';

    dataObj['teamWins2017'] = new Object();
    dataObj['teamWins2017']['data'] = teamWins2017;
    dataObj['teamWins2017']['title'] = 'Teams wins the IPL season 2017'
    dataObj['teamWins2017']['subtitle'] = 'Number of wins of each team in IPL season 2017';
    dataObj['teamWins2017']['tooltip'] = 'won <b>{point.y:.1f} matches in IPL 2017</b>';
    dataObj['teamWins2017']['seriesName'] = 'Team Wins';


    dataObj['teamTotalRuns2015'] = new Object();
    dataObj['teamTotalRuns2015']['data'] = teamTotalRuns2015;
    dataObj['teamTotalRuns2015']['title'] = 'Teams total runs in the IPL season 2015'
    dataObj['teamTotalRuns2015']['subtitle'] = 'Total runs scored by each team in IPL season 2015';
    dataObj['teamTotalRuns2015']['tooltip'] = 'scored <b>{point.y:.1f} runs over IPL 2015</b>';
    dataObj['teamTotalRuns2015']['seriesName'] = 'Team Runs';


    dataObj['teamTotalRuns2016'] = new Object();
    dataObj['teamTotalRuns2016']['data'] = teamTotalRuns2016;
    dataObj['teamTotalRuns2016']['title'] = 'Teams total runs in the IPL season 2016'
    dataObj['teamTotalRuns2016']['subtitle'] = 'Total runs scored by each team in IPL season 2016';
    dataObj['teamTotalRuns2016']['tooltip'] = 'scored <b>{point.y:.1f} runs over IPL 2016</b>';
    dataObj['teamTotalRuns2016']['seriesName'] = 'Team Runs';

    dataObj['teamTotalRuns2017'] = new Object();
    dataObj['teamTotalRuns2017']['data'] = teamTotalRuns2017;
    dataObj['teamTotalRuns2017']['title'] = 'Teams total runs in the IPL season 2017'
    dataObj['teamTotalRuns2017']['subtitle'] = 'Total runs scored by each team in IPL season 2017';
    dataObj['teamTotalRuns2017']['tooltip'] = 'scored <b>{point.y:.1f} runs over IPL 2017</b>';
    dataObj['teamTotalRuns2017']['seriesName'] = 'Team Runs';

    dataObj['topBatsmenRuns2015'] = new Object();
    dataObj['topBatsmenRuns2015']['data'] = topBatsmenRuns2015;
    dataObj['topBatsmenRuns2015']['title'] = 'Top runs in the IPL season 2015'
    dataObj['topBatsmenRuns2015']['subtitle'] = 'Top Batsmen\'s total runs in IPL season 2015';
    dataObj['topBatsmenRuns2015']['tooltip'] = 'scored <b>{point.y:.1f} runs over IPL 2015</b>';
    dataObj['topBatsmenRuns2015']['seriesName'] = 'Batsmen\'s Runs';

    dataObj['topBatsmenRuns2016'] = new Object();
    dataObj['topBatsmenRuns2016']['data'] = topBatsmenRuns2016;
    dataObj['topBatsmenRuns2016']['title'] = 'Top runs in the IPL season 2016'
    dataObj['topBatsmenRuns2016']['subtitle'] = 'Top Batsmen\'s total runs in IPL season 2016';
    dataObj['topBatsmenRuns2016']['tooltip'] = 'scored <b>{point.y:.1f} runs over IPL 2016</b>';
    dataObj['topBatsmenRuns2016']['seriesName'] = 'Batsmen\'s Runs';

    dataObj['topBatsmenRuns2017'] = new Object();
    dataObj['topBatsmenRuns2017']['data'] = topBatsmenRuns2017;
    dataObj['topBatsmenRuns2017']['title'] = 'Top runs in the IPL season 2017'
    dataObj['topBatsmenRuns2017']['subtitle'] = 'Top Batsmen\'s total runs in IPL season 2017';
    dataObj['topBatsmenRuns2017']['tooltip'] = 'scored <b>{point.y:.1f} runs over IPL 2017</b>';
    dataObj['topBatsmenRuns2017']['seriesName'] = 'Batsmen\'s Runs';

    dataObj['bestStrikeRates2015'] = new Object();
    dataObj['bestStrikeRates2015']['data'] = bestStrikeRates2015;
    dataObj['bestStrikeRates2015']['title'] = 'Top strike rates in the IPL season 2015'
    dataObj['bestStrikeRates2015']['subtitle'] = 'Top batsmen by strike rates in IPL season 2015';
    dataObj['bestStrikeRates2015']['tooltip'] = 'had a strike rate of <b>{point.y:.1f} over IPL 2015</b>';
    dataObj['bestStrikeRates2015']['seriesName'] = 'Batsmen\'s strike rates';

    dataObj['bestStrikeRates2016'] = new Object();
    dataObj['bestStrikeRates2016']['data'] = bestStrikeRates2016;
    dataObj['bestStrikeRates2016']['title'] = 'Top strike rates in the IPL season 2016'
    dataObj['bestStrikeRates2016']['subtitle'] = 'Top batsmen by strike rates in IPL season 2016';
    dataObj['bestStrikeRates2016']['tooltip'] = 'had a strike rate of <b>{point.y:.1f} over IPL 2016</b>';
    dataObj['bestStrikeRates2016']['seriesName'] = 'Batsmen\'s strike rates';

    dataObj['bestStrikeRates2017'] = new Object();
    dataObj['bestStrikeRates2017']['data'] = bestStrikeRates2017;
    dataObj['bestStrikeRates2017']['title'] = 'Top strike rates in the IPL season 2017'
    dataObj['bestStrikeRates2017']['subtitle'] = 'Top batsmen by strike rates in IPL season 2017';
    dataObj['bestStrikeRates2017']['tooltip'] = 'had a strike rate of <b>{point.y:.1f} over IPL 2017</b>';
    dataObj['bestStrikeRates2017']['seriesName'] = 'Batsmen\'s strike rates';

    dataObj['bestEconomy2015'] = new Object();
    dataObj['bestEconomy2015']['data'] = bestEconomy2015;
    dataObj['bestEconomy2015']['title'] = 'Top economy rates in the IPL season 2015'
    dataObj['bestEconomy2015']['subtitle'] = 'Top bowlers by economy rates in IPL season 2015';
    dataObj['bestEconomy2015']['tooltip'] = 'had a economy rate of <b>{point.y:.1f} over IPL 2015</b>';
    dataObj['bestEconomy2015']['seriesName'] = 'Bowlers\' economy rates';

    dataObj['bestEconomy2016'] = new Object();
    dataObj['bestEconomy2016']['data'] = bestEconomy2016;
    dataObj['bestEconomy2016']['title'] = 'Top economy rates in the IPL season 2016'
    dataObj['bestEconomy2016']['subtitle'] = 'Top bowlers by economy rates in IPL season 2016';
    dataObj['bestEconomy2016']['tooltip'] = 'had a economy rate of <b>{point.y:.1f} over IPL 2016</b>';
    dataObj['bestEconomy2016']['seriesName'] = 'Bowlers\' economy rates';


    dataObj['bestEconomy2017'] = new Object();
    dataObj['bestEconomy2017']['data'] = bestEconomy2017;
    dataObj['bestEconomy2017']['title'] = 'Top economy rates in the IPL season 2017'
    dataObj['bestEconomy2017']['subtitle'] = 'Top bowlers by economy rates in IPL season 2017';
    dataObj['bestEconomy2017']['tooltip'] = 'had a economy rate of <b>{point.y:.1f} over IPL 2017</b>';
    dataObj['bestEconomy2017']['seriesName'] = 'Bowlers\' economy rates';


    dataObj['mostWickets2015'] = new Object();
    dataObj['mostWickets2015']['data'] = mostWickets2015;
    dataObj['mostWickets2015']['title'] = 'Highest wickets in the IPL season 2015'
    dataObj['mostWickets2015']['subtitle'] = 'Top bowlers by number of wickets in IPL season 2015';
    dataObj['mostWickets2015']['tooltip'] = 'had <b>{point.y:.1f} wickets over IPL 2015</b>';
    dataObj['mostWickets2015']['seriesName'] = 'Bowlers\' total wickets';


    dataObj['mostWickets2016'] = new Object();
    dataObj['mostWickets2016']['data'] = mostWickets2016;
    dataObj['mostWickets2016']['title'] = 'Highest wickets in the IPL season 2016'
    dataObj['mostWickets2016']['subtitle'] = 'Top bowlers by number of wickets in IPL season 2016';
    dataObj['mostWickets2016']['tooltip'] = 'had <b>{point.y:.1f} wickets over IPL 2016</b>';
    dataObj['mostWickets2016']['seriesName'] = 'Bowlers\' total wickets';

    dataObj['mostWickets2017'] = new Object();
    dataObj['mostWickets2017']['data'] = mostWickets2017;
    dataObj['mostWickets2017']['title'] = 'Highest wickets in the IPL season 2017'
    dataObj['mostWickets2017']['subtitle'] = 'Top bowlers by number of wickets in IPL season 2017';
    dataObj['mostWickets2017']['tooltip'] = 'had <b>{point.y:.1f} wickets over IPL 2017</b>';
    dataObj['mostWickets2017']['seriesName'] = 'Bowlers\' total wickets';


    fs.writeFile('../public/assets/data.json', JSON.stringify(dataObj), (err) => {
        if (err) throw err;
        console.log("the \'data.json\' file is created successfully!");
    });

});
