var fs = require('fs')
var deliveriesFile = './JSON/deliveries.json'
var matches = './JSON/dataFromMatches.json'

fs.readFile(matches, 'utf8', (error, matchesData) => {
  if (error) throw error;

  matchesData = JSON.parse(matchesData);
  fs.readFile(deliveriesFile, 'utf8', (error, deliveriesData) => {
    if (error) throw error;

    var dData = JSON.parse(deliveriesData);
    var ids = matchesData.idsSet;

    function extract(forEvery, fromWhich, check, pivot, incBy) {
      var data = forEvery.map(year => {
        return fromWhich.filter(row => {
            if ((Number(row[0]) >= Number(year[1])) && (Number(row[0]) <= Number(year[2]))) {
              if ((!(Number(row[check[0]]) < 1)) && (row[check[1]] !== '0' || '')) {
                return true;
              }
            }
            return false;
          })
          .reduce((acc, row) => {
            if (!acc.hasOwnProperty(year[0])) {
              acc[year[0]] = {};
            }
            if (!acc[year[0]].hasOwnProperty(row[pivot])) {
              acc[year[0]][row[pivot]] = {};
            }
            acc[year[0]][row[pivot]]['runs'] = (acc[year[0]][row[pivot]]['runs'] || 0) + Number(row[incBy]);
            acc[year[0]][row[pivot]]['balls'] = (acc[year[0]][row[pivot]]['balls'] || 0) + 1;
            acc[year[0]][row[pivot]]['strikeRate'] = ((acc[year[0]][row[pivot]]['runs'] || 1) / acc[year[0]][row[pivot]]['balls']) * 100;
            return acc;
          }, {});
      });
      return data;
    }

    // var teamsTotalRuns = extract(ids, dData, [17, 2], 2, 17);

    var batsmenRuns = extract(ids, dData, [15, 6], 6, 15);

    console.log(batsmenRuns[0]);



    // fs.writeFile('./JSON/data.json', JSON.stringify(data), (err) => {
    //     if (err) throw err;
    //     console.log('The \'data.json\' file has been saved!');
    // });
  }) /*2nd read*/
});