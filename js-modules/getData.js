let fs = require('fs');

let team = './JSON/teamsData.json';
let bat = './JSON/battingdata.json';
let ball = './JSON/bowlingData.json';

fs.readFile(team, "utf8", (err, tData) => {
  if (err) throw err;
  let team = JSON.parse(tData);
  fs.readFile(bat, "utf8", (err, btData) => {
    if (err) throw err;
    let bat = JSON.parse(btData);
    fs.readFile(ball, "utf8", (err, blData) => {
      if (err) throw err;
      let ball = JSON.parse(blData);

      let data = {};
      for (const year in team) {
        if (team.hasOwnProperty(year)) {
          data[year] = {
            teams: {},
            bat: {},
            ball: {}
          };
          data[year]['teams']['runs'] = team[year]['runs'];
          data[year]['teams']['wins'] = team[year]['wins'];
          data[year]['bat']['runs'] = bat[year]['runs'];
          data[year]['bat']['strikeRate'] = bat[year]['strikeRate'];
          data[year]['ball']['wickets'] = ball[year]['wickets'];
          data[year]['ball']['economyRate'] = ball[year]['economyRate'];
        }
      }

      fs.writeFile('../public/assets/data.json', JSON.stringify(data), (err) => {
        console.log('data.json created successfully.')
      })
    })
  })
})