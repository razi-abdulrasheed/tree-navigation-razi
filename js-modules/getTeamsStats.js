var fs = require('fs')
var deliveriesFile = './JSON/deliveries.json'
var matches = './JSON/dataFromMatches.json'

fs.readFile(matches, 'utf8', (error, matches) => {
  if (error) throw error;

  matchesData = JSON.parse(matches);

  fs.readFile(deliveriesFile, 'utf8', (error, deliveriesData) => {
    if (error) throw error;

    var dData = JSON.parse(deliveriesData);
    var ids = matchesData.idsSet;

    function extract(forEvery, find, fromWhich, check, pivot, incBy) {
      var data = {};
      forEvery.forEach(year => {
        data[year[0]] = fromWhich.filter(row => {
            if ((Number(row[0]) >= Number(year[1])) && (Number(row[0]) <= Number(year[2]))) {
              if ((!(Number(row[check[0]]) < 1)) && (row[check[1]] !== '0' || '')) {
                return true;
              }
            }
            return false;
          })
          .reduce((acc, row) => {
            if (!acc.hasOwnProperty(find))
              acc[find] = {};
            acc[find][row[pivot]] = (acc[find][row[pivot]] || 0) + Number(row[incBy]);;
            return acc;
          }, {});
      });
      return data;
    }

    var teams = extract(ids, 'runs', dData, [17, 2], 2, 17);

    for (const year in teams) {
      if (teams.hasOwnProperty(year)) {
        teams[year]['wins'] = matchesData[year]['wins'];
      }
    }

    fs.writeFile('./JSON/teamsData.json', JSON.stringify(teams), (err) => {
      if (err) throw err;
      console.log('The \'teamsData.json\' file has been saved!');
    });
  }) /*2nd read*/
});