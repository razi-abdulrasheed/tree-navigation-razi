var fs = require('fs')
var matchesFile = './JSON/matches.json'

fs.readFile(matchesFile, 'utf8', (error, matchesData) => {
  if (error) throw error;
  rawMatchesData = JSON.parse(matchesData);

  var data = rawMatchesData.reduce(function(acc, row) {
    if (acc.hasOwnProperty(row[1])) {
      acc[row[1]]['id'][1] = row[0];
      acc[row[1]]['wins'][row[10]] = (acc[row[1]]['wins'][row[10]] || 0) + 1;
    } else {
      acc[row[1]] = {};
      acc[row[1]]['id'] = [row[0], 0];
      acc[row[1]]['wins'] = {};
    }
    return acc;
  }, {});
  console.log(data);

  var idsSet = [];
  for (var year in data) {
    idsSet.push([`${year}`, data[year]['id'][0], data[year]['id'][1]]);
  }
  console.log(idsSet);
  data['idsSet'] = idsSet;
  fs.writeFile('./JSON/dataFromMatches.json', JSON.stringify(data), (err) => {
    if (err) throw err;
    console.log('The \'dataFromMatches.json\' file has been saved!');
  });
});