var fs = require('fs');
const matches = './data-set/matches.csv';
const csv = require('csvtojson');
var matchesDetailedList = [];

csv({
        workerNum: 4,
        trim: true,
    })
    .fromFile(matches)
    .on('csv', (match) => {
        matchesDetailedList.push(match);
    })
    .on('done', (error) => {
        fs.writeFile('./JSON/matches.json', JSON.stringify(matchesDetailedList), (err) => {
            if (err) throw err;
            console.log('The file has been saved!');
        });
        console.log('\'matches.json\' has built successfully.');
    })
