var fs = require('fs')
var deliveriesFile = './JSON/deliveries.json'
var matches = './JSON/dataFromMatches.json'

fs.readFile(matches, 'utf8', (error, matchesData) => {
  if (error) throw error;

  matchesData = JSON.parse(matchesData);
  fs.readFile(deliveriesFile, 'utf8', (error, deliveriesData) => {
    if (error) throw error;

    var dData = JSON.parse(deliveriesData);
    var ids = matchesData.idsSet;

    function extract(forEvery, fromWhich, pivot, incBy) {
      var data = {};
      forEvery.forEach(year => {
        data[year[0]] = fromWhich.filter(row => {
            if ((Number(row[0]) >= Number(year[1])) && (Number(row[0]) <= Number(year[2]))) {
              return true;
            }
            return false;
          })
          .reduce((acc, row) => {
            if (!acc.hasOwnProperty(row[pivot])) {
              acc[row[pivot]] = {};
            }
            acc[row[pivot]]['runs'] = (acc[row[pivot]]['runs'] || 0) + Number(row[incBy]);
            acc[row[pivot]]['balls'] = (acc[row[pivot]]['balls'] || 0) + 1;
            acc[row[pivot]]['economyRate'] = ((acc[row[pivot]]['runs'] || 1) / (acc[row[pivot]]['balls'] / 6));
            if ((row[19] !== '') && (row[19] !== 'run out')) {
              acc[row[pivot]]['wickets'] = (acc[row[pivot]]['wickets'] || 0) + 1;
            }

            return acc;
          }, {});
      });
      return data;
    }

    var bowling = extract(ids, dData, 8, 17);

    var bData = {};

    for (const year in bowling) {
      if (bowling.hasOwnProperty(year)) {
        bData[year] = {
          wickets: {},
          economyRate: {}
        };
        for (const player in bowling[year]) {
          if (bowling[year].hasOwnProperty(player)) {
            if (bowling[year][player]['wickets'] > 17) {
              bData[year]['wickets'][player] = bowling[year][player]['wickets'];
            }
            if (bowling[year][player]['economyRate'] < 6.5) {
              bData[year]['economyRate'][player] = bowling[year][player]['economyRate'];
            }
          }
        }
      }
    }

    fs.writeFile('./JSON/bowlingData.json', JSON.stringify(bData), (err) => {
      if (err) throw err;
      console.log('The \'bowlingData.json\' file has been saved!');
    });
  }) /*2nd read*/
});