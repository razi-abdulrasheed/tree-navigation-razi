var fs = require('fs')
var deliveriesFile = './JSON/deliveries.json'
var matches = './JSON/dataFromMatches.json'

fs.readFile(matches, 'utf8', (error, matchesData) => {
  if (error) throw error;

  matchesData = JSON.parse(matchesData);
  fs.readFile(deliveriesFile, 'utf8', (error, deliveriesData) => {
    if (error) throw error;

    var dData = JSON.parse(deliveriesData);
    var ids = matchesData.idsSet;

    function extract(forEvery, fromWhich, pivot, incBy) {
      var data = {};
      forEvery.forEach(year => {
        data[year[0]] = fromWhich.filter(row => {
            if ((Number(row[0]) >= Number(year[1])) && (Number(row[0]) <= Number(year[2]))) {
              return true;
            }
            return false;
          })
          .reduce((acc, row) => {
            if (!acc.hasOwnProperty(row[pivot])) {
              acc[row[pivot]] = {};
            }
            acc[row[pivot]]['runs'] = (acc[row[pivot]]['runs'] || 0) + Number(row[incBy]);
            acc[row[pivot]]['balls'] = (acc[row[pivot]]['balls'] || 0) + 1;
            acc[row[pivot]]['strikeRate'] = ((acc[row[pivot]]['runs'] || 1) / acc[row[pivot]]['balls']) * 100;
            return acc;
          }, {});
      });
      return data;
    }

    var batsmen = extract(ids, dData, 6, 15);
    var bData = {};

    for (const year in batsmen) {
      if (batsmen.hasOwnProperty(year)) {
        bData[year] = {
          runs: {},
          strikeRate: {}
        };
        for (const player in batsmen[year]) {
          if (batsmen[year].hasOwnProperty(player)) {
            if (batsmen[year][player]['runs'] > 400) {
              bData[year]['runs'][player] = batsmen[year][player]['runs'];
            }
            if (batsmen[year][player]['strikeRate'] > 170) {
              bData[year]['strikeRate'][player] = batsmen[year][player]['strikeRate'];
            }
          }
        }
      }
    }

    fs.writeFile('./JSON/battingdata.json', JSON.stringify(bData), (err) => {
      if (err) throw err;
      console.log('The \'battingata.json\' file has been saved!');
    });
  }) /*2nd read*/
});