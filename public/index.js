$("dl.decision-tree dd, dl.decision-tree dt").addClass("collapsed");
$("dl.decision-tree dt").click(function(event) {
  $(event.target).toggleClass("collapsed");
  $(event.target).next().toggleClass("collapsed");
});

$(document).ready(function() {
  $.ajax({
    type: "GET",
    url: "./assets/data.json",
    success: function(data) {
      $("dl.decision-tree dt").on("click", function() {
        if ($(this).attr("id") !== undefined) {
          var id = $(this).attr("id");
          id = id.split('-')
          var subt = {
            teams: 'Teams',
            bat: 'Batting',
            ball: 'Bowling'
          }
          var chart = Highcharts.chart('container', {

            title: {
              text: `Statistics of IPL season ${id[0]}.`
            },

            subtitle: {
              text: subt[id[1]] + ' statistics.'
            },

            xAxis: {
              categories: Object.keys(data[id[0]][id[1]][id[2]])
            },

            series: [{
              type: 'column',
              colorByPoint: true,
              data: Object.values(data[id[0]][id[1]][id[2]]),
              showInLegend: false
            }]

          }); /*  closing of HighCharts  */
        } /*  closing of if condition  */
      }) /*  closing of click function  */
    } /*  closing of ajax.success  */
  }) /*  closing of ajax  */
}) /*  closing of on document.ready  */