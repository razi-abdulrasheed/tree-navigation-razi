- The tree navigation is implemented using Description list tags of HTML, display property of CSS and jQuery.
- The scripts in 'js-module' directory extract and transform the CSV file and provide data for visualization.
- The data visualization uses HighCharts API.
- Charts are dynamically depicted by using jQuery event listeners and DOM manipulation.

